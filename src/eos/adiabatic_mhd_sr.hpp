#ifndef EOS_ADIABATIC_MHD_SR_HPP_
#define EOS_ADIABATIC_MHD_SR_HPP_
//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details

KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real[]) const {return 0.0;}

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real[], const Real) const {return 0.0;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) const {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &prim,
  const AthenaArray<Real> &bbx_vals, int k, int j, int il, int iu, int ivx,
  AthenaArray<Real> &lambdas_p, AthenaArray<Real> &lambdas_m) const;

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *) const
  {return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
  Real *, Real *) const {return;}

#endif // EOS_ADIABATIC_MHD_SR_HPP_
