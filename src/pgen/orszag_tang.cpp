//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file orszag-tang.c
//  \brief Problem generator for Orszag-Tang vortex problem.
//
// REFERENCE: For example, see: G. Toth,  "The div(B)=0 constraint in shock capturing
//   MHD codes", JCP, 161, 605 (2000)
//========================================================================================

// C/C++ headers
#include <cmath>      // sqrt()
#include <iostream>   // endl
#include <sstream>    // stringstream
#include <stdexcept>  // runtime_error
#include <string>     // c_str()

// Athena++ headers
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../parameter_input.hpp"
#include "../coordinates/coordinates.hpp"
#include "../eos/eos.hpp"
#include "../field/field.hpp"
#include "../hydro/hydro.hpp"
#include "../mesh/mesh.hpp"

#if !MAGNETIC_FIELDS_ENABLED
#error "This problem generator requires magnetic fields"
#endif


//========================================================================================
//! \fn void MeshBlock::ProblemGenerator(ParameterInput *pin)
//  \brief Problem Generator for the Orszag-Tang test.  The initial conditions are
//  constructed assuming the domain extends over [-0.5x0.5, -0.5x0.5], so that exact
//  symmetry can be enforced across x=0 and y=0.
//========================================================================================

void MeshBlock::ProblemGenerator(ParameterInput *pin) {
  Real gm1 = peos->GetGamma() - 1.0;

  int nx1 = (ie-is)+1 + 2*(NGHOST);
  int nx2 = (je-js)+1 + 2*(NGHOST);
  Kokkos::View<Real**,Kokkos::LayoutRight,HostSpace> az("az",nx2,nx1);
  
  //Make some temporary arrays to do everything here on the host
  //(out-scopes the class members)
  auto x1v = Kokkos::create_mirror_view(pcoord->x1v.get_KView1D());
  auto x2v = Kokkos::create_mirror_view(pcoord->x2v.get_KView1D());
  Kokkos::deep_copy(x1v,pcoord->x1v.get_KView1D());
  Kokkos::deep_copy(x2v,pcoord->x2v.get_KView1D());
    
  auto x1f = Kokkos::create_mirror_view(pcoord->x1f.get_KView1D());
  auto x2f = Kokkos::create_mirror_view(pcoord->x2f.get_KView1D());
  Kokkos::deep_copy(x1f,pcoord->x1f.get_KView1D());
  Kokkos::deep_copy(x2f,pcoord->x2f.get_KView1D());
   
  auto dx1f = Kokkos::create_mirror_view(pcoord->dx1f.get_KView1D());
  auto dx2f = Kokkos::create_mirror_view(pcoord->dx2f.get_KView1D());
  Kokkos::deep_copy(dx1f,pcoord->dx1f.get_KView1D());
  Kokkos::deep_copy(dx2f,pcoord->dx2f.get_KView1D());
  
  auto b_x1f = Kokkos::create_mirror_view(pfield->b.x1f.get_KView3D());
  auto b_x2f = Kokkos::create_mirror_view(pfield->b.x2f.get_KView3D());
  auto b_x3f = Kokkos::create_mirror_view(pfield->b.x3f.get_KView3D());
  auto u = Kokkos::create_mirror_view(phydro->u.get_KView4D());

  Real B0 = 1.0/std::sqrt(4.0*PI);
  Real d0 = 25.0/(36.0*PI);
  Real v0 = 1.0;
  Real p0 = 5.0/(12.0*PI);

  // Initialize vector potential
  for (int j=js; j<=je+1; ++j) {
  for (int i=is; i<=ie+1; ++i) {
    az(j,i) = B0/(4.0*PI)*(cos(4.0*PI*x1f(i)) - 2.0*cos(2.0*PI*x2f(j)));
  }}

  // Initialize density, momentum, face-centered fields
  for (int k=ks; k<=ke; k++) {
  for (int j=js; j<=je; j++) {
  for (int i=is; i<=ie; i++) {
    u(IDN,k,j,i) = d0;
    u(IM1,k,j,i) =  d0*v0*sin(2.0*PI*x2v(j));
    u(IM2,k,j,i) = -d0*v0*sin(2.0*PI*x1v(i));
    u(IM3,k,j,i) = 0.0;
  }}}

  // initialize interface B
  for (int k=ks; k<=ke; k++) {
  for (int j=js; j<=je; j++) {
  for (int i=is; i<=ie+1; i++) {
    b_x1f(k,j,i) = (az(j+1,i) - az(j,i))/dx2f(j);
  }}}
  for (int k=ks; k<=ke; k++) {
  for (int j=js; j<=je+1; j++) {
  for (int i=is; i<=ie; i++) {
    b_x2f(k,j,i) = (az(j,i) - az(j,i+1))/dx1f(i);
  }}}
  for (int k=ks; k<=ke+1; k++) {
  for (int j=js; j<=je; j++) {
  for (int i=is; i<=ie; i++) {
    b_x3f(k,j,i) = 0.0;
  }}}

  // initialize total energy
  if (NON_BAROTROPIC_EOS) {
    for (int k=ks; k<=ke; k++) {
    for (int j=js; j<=je; j++) {
    for (int i=is; i<=ie; i++) {
      u(IEN,k,j,i) = p0/gm1 +
          0.5*(SQR(0.5*(b_x1f(k,j,i) + b_x1f(k,j,i+1))) +
               SQR(0.5*(b_x2f(k,j,i) + b_x2f(k,j+1,i))) +
               SQR(0.5*(b_x3f(k,j,i) + b_x3f(k+1,j,i)))) + (0.5)*
          (SQR(u(IM1,k,j,i)) + SQR(u(IM2,k,j,i))
           + SQR(u(IM3,k,j,i)))/u(IDN,k,j,i);
    }}}
  }
  
  //Copy vars back to device memory
  Kokkos::deep_copy(pfield->b.x1f.get_KView3D(),b_x1f);
  Kokkos::deep_copy(pfield->b.x2f.get_KView3D(),b_x2f);
  Kokkos::deep_copy(pfield->b.x3f.get_KView3D(),b_x3f);
  Kokkos::deep_copy(phydro->u.get_KView4D(),u);

  return;
}
